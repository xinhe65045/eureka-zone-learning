package org.xinhe.scl.pay;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@EnableSwagger2Doc
@SpringBootApplication
public class PayServiceApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(PayServiceApplication.class).web(true).run(args);
	}

}
