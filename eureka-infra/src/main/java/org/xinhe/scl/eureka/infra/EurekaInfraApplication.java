package org.xinhe.scl.eureka.infra;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaInfraApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(EurekaInfraApplication.class).web(true).run(args);
	}

}
