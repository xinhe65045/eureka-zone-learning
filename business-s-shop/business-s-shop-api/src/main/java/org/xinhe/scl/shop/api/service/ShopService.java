package org.xinhe.scl.shop.api.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>Title: ShopService</p>
 * <p>Date: 2018/10/17 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@FeignClient(value = "shop-service")
public interface ShopService {

    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    void buy(@RequestParam("orderNo") String orderNo);

}
