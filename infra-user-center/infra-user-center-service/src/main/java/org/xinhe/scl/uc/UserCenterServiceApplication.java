package org.xinhe.scl.uc;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@EnableSwagger2Doc
@SpringBootApplication
public class UserCenterServiceApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(UserCenterServiceApplication.class).web(true).run(args);
	}

}
