package org.xinhe.scl.eureka.business.g;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaBusinessGApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(EurekaBusinessGApplication.class).web(true).run(args);
	}

}
