package org.xinhe.scl.eureka.business.s;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaBusinessSApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(EurekaBusinessSApplication.class).web(true).run(args);
	}

}
