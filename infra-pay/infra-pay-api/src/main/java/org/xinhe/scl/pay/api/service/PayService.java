package org.xinhe.scl.pay.api.service;

import org.springframework.web.bind.annotation.RequestParam;
import org.xinhe.scl.pay.api.dto.PayInfoDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>Title: PayService</p>
 * <p>Date: 2018/10/17 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@FeignClient(value = "pay-service")
public interface PayService {

    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    void pay(@RequestParam("orderNo") String orderNo);

}
