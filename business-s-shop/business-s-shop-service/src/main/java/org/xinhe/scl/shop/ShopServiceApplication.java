package org.xinhe.scl.shop;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@EnableDiscoveryClient
@EnableSwagger2Doc
@EnableFeignClients("org.xinhe.scl")
@SpringBootApplication
public class ShopServiceApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(ShopServiceApplication.class).web(true).run(args);
	}

}
