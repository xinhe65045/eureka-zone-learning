package org.xinhe.scl.shop.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaClientConfigBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.xinhe.scl.uc.api.dto.UserInfoDTO;
import org.xinhe.scl.uc.api.service.UserService;

@RestController
public class ShopController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    @Autowired
    EurekaClientConfigBean eurekaClientConfigBean;

    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public void buy(@RequestParam("orderNo") String orderNo) {
        logger.info("buy orderNod = {}", orderNo);
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.setUnionId("111");
        UserInfoDTO byUnionId = userService.getByUnionId(userInfoDTO);

        logger.info("userInfoDTO = {}", byUnionId);
    }

}