package org.xinhe.scl.uc.api.dto;

import java.io.Serializable;

/**
 * <p>Title: UserInfoDTO</p>
 * <p>Date: 2018/10/17 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
public class UserInfoDTO implements Serializable {

    private static final long serialVersionUID = 8461274111235487567L;

    /**
     * 全局用户ID
     */
    private String unionId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 手机号
     */
    private String mobile;

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "UserInfoDTO{" +
                "unionId='" + unionId + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
