package org.xinhe.scl.pay.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaClientConfigBean;
import org.springframework.web.bind.annotation.*;

@RestController
public class PayController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    EurekaClientConfigBean eurekaClientConfigBean;

    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    public void pay(@RequestParam("orderNo") String orderNo) {
        logger.info("pay orderNod = {}", orderNo);
    }

}