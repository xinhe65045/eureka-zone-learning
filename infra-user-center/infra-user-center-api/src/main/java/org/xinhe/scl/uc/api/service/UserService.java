package org.xinhe.scl.uc.api.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xinhe.scl.uc.api.dto.UserInfoDTO;

/**
 * <p>Title: UserService</p>
 * <p>Date: 2018/10/17 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@FeignClient(value = "user-service")
public interface UserService {

    @RequestMapping(value = "/user/get/by-union-id", method = RequestMethod.POST)
    UserInfoDTO getByUnionId(@RequestBody UserInfoDTO userInfoDTO);

}
