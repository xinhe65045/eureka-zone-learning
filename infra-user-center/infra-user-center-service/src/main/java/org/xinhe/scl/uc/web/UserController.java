package org.xinhe.scl.uc.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaClientConfigBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xinhe.scl.uc.api.dto.UserInfoDTO;

@RestController
@RequestMapping("/user")
public class UserController {

    private final Logger logger = Logger.getLogger(getClass());

    @Autowired
    EurekaClientConfigBean eurekaClientConfigBean;

    @RequestMapping(value = "/get/by-union-id", method = RequestMethod.POST)
    public UserInfoDTO getByUnionId(@RequestBody UserInfoDTO userInfoDTO) {
        String unionId = userInfoDTO.getUnionId();
        UserInfoDTO user = new UserInfoDTO();
        user.setUnionId(unionId);
        return user;
    }

}