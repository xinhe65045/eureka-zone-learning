package org.xinhe.scl.uc.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Title: UserCenterApiAutoConfiguration</p>
 * <p>Date: 2018/10/22 </p>
 * <p>Description: </p>
 *
 * @author sunxinhe
 */
@Configuration
@ComponentScan({"org.xinhe.scl.uc.api"})
public class UserCenterApiAutoConfiguration {

}
